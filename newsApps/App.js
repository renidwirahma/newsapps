import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Navigator from './app/modules/RootNavigator/Navigator';

const App = () => {
  return (
    <NavigationContainer>
      <Navigator></Navigator>
    </NavigationContainer>
  )
}

export default App