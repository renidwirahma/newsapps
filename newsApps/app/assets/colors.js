const colors = {
  bg:'#FBFBFB',
  grey1:'#F1F1F1',
  grey2:'#C4C4C4',
  blackTransparent: '#00000080',
  greyTransparent:'#FBFBFB90',
  blue:"#1A30FF",
};

export default colors;
