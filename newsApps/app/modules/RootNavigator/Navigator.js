import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ListNews from '../MainMenu/ListNews';
import DetailNewsScreen from '../MainMenu/screens/DetailNewsScreen';

const Stack = createStackNavigator();

const Navigator = () => {
  return (
    <Stack.Navigator initialRouteName="ListNews">
      <Stack.Screen
        name="ListNews"
        component={ListNews}
        options={{headerShown: false}}></Stack.Screen>

      <Stack.Screen
        name="DetailNewsScreen"
        component={DetailNewsScreen}
        options={{headerShown: false}}></Stack.Screen>
    </Stack.Navigator>
  );
};

export default Navigator;

const styles = StyleSheet.create({});
