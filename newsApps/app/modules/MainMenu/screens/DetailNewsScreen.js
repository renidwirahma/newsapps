import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  Linking,
  TouchableOpacity,
  ScrollView, Alert
} from 'react-native';
import moment from 'moment';
import React from 'react';
import NetInfo from '@react-native-community/netinfo';
import colors from '../../../assets/colors';
import ButtonBack from '../../../components/ButtonBack';
import fonts from '../../../assets/fonts';

class DetailNewsScreen extends React.Component {
  render() {
    const item = this.props.route.params.item;
    const unsubscribe = NetInfo.addEventListener((state) => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });

    // Unsubscribe
    unsubscribe();
    NetInfo.refresh().then((state) => {
      if (state.isConnected === false) {
        Alert.alert('Connection Lost');
      }
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });

    return (
      <View style={styles.bg}>
        <ImageBackground source={{uri: item.urlToImage}} style={styles.img}>
          <ButtonBack
            onPress={() => this.props.navigation.goBack()}></ButtonBack>
        </ImageBackground>
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.bgSource}>
              <Text style={styles.txtSource}>{item.source.name}</Text>
            </View>

            <Text style={styles.txtTitle}>{item.title}</Text>
            <Text style={styles.txtAuthor}>
              {item.author} | {moment(item.publishedAt).format('MMMM DD, YYYY')}
            </Text>
            <Text style={styles.txtDesc}>{item.description}</Text>
            <Text style={styles.txtContent}>{item.content}</Text>
            <TouchableOpacity onPress={() => Linking.openURL(item.url)}>
              <Text style={styles.txtLink}>{item.url}</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default DetailNewsScreen;

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    backgroundColor: colors.bg,
  },
  img: {
    width: '100%',
    height: '60%',
    resizeMode: 'contain',
  },
  container: {
    backgroundColor: 'white',
    marginTop: '-60%',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    flex: 1,
    paddingTop: 30,
    paddingHorizontal: 20,
  },
  bgSource: {
    padding: 7,
    borderRadius: 20,
    backgroundColor: colors.grey1,
    width: '50%',
    justifyContent: 'center',
  },
  txtSource: {
    fontSize: 12,
    fontFamily: fonts.regular,
    textAlign: 'center',
  },
  txtTitle: {
    fontSize: 20,
    fontFamily: fonts.bold,
    marginTop: 20,
  },
  txtAuthor: {
    fontSize: 12,
    fontFamily: fonts.regular,
    marginBottom: 10,
    marginTop: 5,
    color: colors.grey2,
  },
  txtDesc: {
    fontSize: 14,
    fontFamily: fonts.regular,
    marginBottom: 10,
    marginTop: 30,
    color: 'black',
  },
  txtContent: {
    fontSize: 14,
    fontFamily: fonts.regular,
    marginBottom: 10,
    color: 'black',
  },
  txtLink: {
    fontSize: 14,
    fontFamily: fonts.regular,
    marginBottom: 10,
    color: colors.blue,
  },
});
