import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Modal,
} from 'react-native';
import React from 'react';
import moment from 'moment';
import NetInfo from '@react-native-community/netinfo';
import colors from '../../../assets/colors';
import fonts from '../../../assets/fonts';
import ItemCity from '../../../components/ItemCity';
import ItemNews from '../../../components/ItemNews';
import Headline from '../../../components/Headline';
import Loading from '../../../components/Loading';

class ListNewsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      itemHeadline: this.props.headline,
    };
  }

  componentDidMount() {
    let arr = this.props.cityData.map((item, index) => {
      item.isClicked = false;
      return {...item};
    });
    this.handleCity('us');
  }

  handleCity = (idn) => {
    let arr = this.props.cityData.map((item) => {
      if (idn === item.id) {
        item.isClicked = !item.isClicked;
        console.log('idn', idn);
        this.props.getNews(idn);
      } else {
        item.isClicked = false;
      }
      return {...item};
    });
    this.setState({list: arr});
  };

  ifNull() {
    if (this.props.newsData.length === 0) {
      return (
        <View style={styles.wrapNull}>
          <Text style={styles.txtNull}>No data</Text>
        </View>
      );
    }
  }

  render() {
    let item = this.props.headline;

    return (
      <View style={styles.bg}>
        <Text style={styles.txtHeader}>Buletin News</Text>
        <View style={styles.bgListCity}>
          <FlatList
            data={this.props.cityData}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => item.id}
            renderItem={({item}) => (
              <View>
                {item.isClicked && (
                  <ItemCity
                    colors={'white'}
                    city={item.city}
                    onPress={() => this.handleCity(item.id)}></ItemCity>
                )}
                {!item.isClicked && (
                  <ItemCity
                    colors={colors.grey1}
                    city={item.city}
                    onPress={() => this.handleCity(item.id)}></ItemCity>
                )}
              </View>
            )}></FlatList>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          onMomentumScrollEnd={() => this.props.loadMore()}>
          {this.props.newsData.length != 0 && (
            <Headline
              title={this.props.headline.title}
              desc={this.props.headline.description}
              img={this.props.headline.urlToImage}
              onPress={() =>
                this.props.navigation.navigate('DetailNewsScreen', {item})
              }></Headline>
          )}

          {this.props.newsData.map((item, k) => {
            let published = moment(item.publishedAt);
            return (
              <ItemNews
                onPress={() =>
                  this.props.navigation.navigate('DetailNewsScreen', {item})
                }
                title={item.title}
                img={item.urlToImage}
                source={item.source.name}
                date={moment(published).format('MMMM DD, YYYY')}
                k={item.url}></ItemNews>
            );
          })}
          {this.ifNull()}
        </ScrollView>
        <Loading isLoading={this.props.isLoading}></Loading>
      </View>
    );
  }
}

export default ListNewsScreen;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: colors.bg,
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 30,
  },
  txtHeader: {
    fontFamily: fonts.bold,
    fontSize: 20,
  },
  bgListCity: {
    borderRadius: 20,
    backgroundColor: colors.grey1,
    height: 50,
    marginTop: 25,
    padding: 5,
  },
  wrapNull: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 170,
  },
  txtNull: {
    textAlign: 'center',
  },
});
