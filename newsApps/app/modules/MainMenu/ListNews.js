import {Alert, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ListNewsScreen from './screens/ListNewsScreen';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import NetInfo from '@react-native-community/netinfo';

class ListNews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cityData: [
        {
          id: 'us',
          city: 'United State',
        },
        {
          id: 'uk',
          city: 'United Kingdom',
        },
        {
          id: 'au',
          city: 'Australia',
        },
        {
          id: 'sg',
          city: 'Singapore',
        },
      ],
      newsData: [],
      headline: [],
      isLoading: false,
      page: 2,
      totalPage: 1,
      cityName: 'us',
      totalResult: 1,
    };
  }

  getNews = (city) => {
    this.setState({
      isLoading: true,
      cityName: city,
    });

    const url =
      'https://newsapi.org/v2/top-headlines?country=' +
      city +
      '&page=1&pageSize=10&apiKey=eddb4fa408c24f66a8446e3db3434add';

    axios
      .get(url, console.log('url', url))
      .then((result) => {
        this.setState({isLoading: false});
        if (result.data.status === 'ok') {
          this.setState({
            newsData: result.data.articles,
          });

          if (result.data.articles.length > 0) {
            this.setState({
              headline: result.data.articles[3],
            });
          }
          let page = result.data.totalResults / 10;
          if (page > parseInt(page)) {
            this.setState({totalPage: parseInt(page) + 1});
            console.log('masuk totall');
          }
          this.setState({page: 2});
        } else {
          Toast.show(result.data.message, Toast.LONG);
          console.log('Get news Error Response: ', result.data.status);
        }
      })
      .catch((error) => {
        Toast.show(error.toString(), Toast.LONG);
        console.log('news api error', error);
      });
  };

  loadMore = () => {
    console.log('masuk load more, page= ', this.state.page);
    if (this.state.page <= this.state.totalPage) {
      this.setState({isLoading: true});
      this.setState((prevState) => ({page: this.state.page + 1}));
      console.log(this.state.page, ' dari ', this.state.totalPage);
      let url =
        'https://newsapi.org/v2/top-headlines?country=' +
        this.state.cityName +
        '&page=' +
        this.state.page +
        '&pageSize=10&apiKey=eddb4fa408c24f66a8446e3db3434add';

      axios
        .get(url, console.log('url load more', url))
        .then((result) => {
          this.setState({isLoading: false});
          this.setState({
            newsData: this.state.newsData.concat(result.data.articles),
          });
        })
        .catch((error) => {
          this.setState({isLoading: false});
          console.log('error on list news more', error);
        });
    }
  };

  render() {
    const unsubscribe = NetInfo.addEventListener((state) => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });

    // Unsubscribe
    unsubscribe();
    NetInfo.refresh().then((state) => {
      if (state.isConnected === false) {
        Alert.alert('Connection Lost');
      }
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
    });
    return (
      <ListNewsScreen
        navigation={this.props.navigation}
        cityData={this.state.cityData}
        headline={this.state.headline}
        newsData={this.state.newsData}
        getNews={this.getNews}
        isLoading={this.state.isLoading}
        loadMore={this.loadMore}></ListNewsScreen>
    );
  }
}

export default ListNews;

const styles = StyleSheet.create({});
