import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import fonts from '../assets/fonts';

class ItemCity extends React.Component {
  handlePress = () => {
    this.props.onPress?.();
  };
  
  render() {
    return (
      <TouchableOpacity style={styles.btn(this.props.colors)} onPress={() => this.handlePress()}>
        <Text style={styles.txtCity}>{this.props.city}</Text>
      </TouchableOpacity>
    );
  }
}

ItemCity.defaultProps = {
  city:"",
  colors:'white',
}

export default ItemCity;

const styles = StyleSheet.create({
  btn: (colors) => ({
    backgroundColor:colors,
    paddingVertical:10,
    paddingHorizontal:25,
    borderRadius:25,
    height:40,
  }),
  txtCity:{
    fontSize:12,
    fontFamily:fonts.semiBold
  },
});
