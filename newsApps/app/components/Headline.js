import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import colors from '../assets/colors';
import fonts from '../assets/fonts';

class Headline extends React.Component {
  
  handlePress = () => {
    this.props.onPress?.();
  };

  render() {
    return (
      <TouchableOpacity onPress={() => this.handlePress()}>
        <ImageBackground
          style={styles.contentTopNews}
          source={{uri: this.props.img}}>
          <View style={styles.wrapTitle}>
            <Text
              ellipsizeMode={'tail'}
              numberOfLines={2}
              style={styles.txtTitle}>
              {this.props.title}
            </Text>
            <Text
              ellipsizeMode={'tail'}
              numberOfLines={2}
              style={styles.txtDesc}>
              {this.props.desc}
            </Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

Headline.defaultProps = {
  title: '',
  desc: '',
  img: 'https://cdn.primedia.co.za/primedia-broadcasting/image/upload/c_fill,h_290,w_480/aixp8e9foaejqyqcvsy7.jpg',
};

export default Headline;

const styles = StyleSheet.create({
  contentTopNews: {
    marginTop: 20,
    width: '100%',
    height: 230,
    justifyContent: 'flex-end',
    marginBottom: 30,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  wrapTitle: {
    backgroundColor: colors.grey2,
    height: '45%',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  txtTitle: {
    color: 'black',
    fontFamily: fonts.semiBold,
    fontSize: 15,
    marginBottom: 5,
  },
  txtDesc: {
    color: 'black',
    fontFamily: fonts.regular,
    fontSize: 10,
  },
});
