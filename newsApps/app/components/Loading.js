import {StyleSheet, Text, View, Modal} from 'react-native';
import React from 'react';
import {TextLoader, BubblesLoader} from 'react-native-indicator';
import colors from '../assets/colors';

class Loading extends React.Component {
  render() {
    return (
      <Modal
        transparent={true}
        visible={this.props.isLoading}
        animationType="fade"
        backdropColor={colors.blackTransparent}>
        <View style={styles.contModal}>
          <View style={styles.containerContentInAlert}>
            <BubblesLoader></BubblesLoader>
            <TextLoader text="Loading" />
          </View>
        </View>
      </Modal>
    );
  }
}

Loading.defaultProps = {
  isLoading:false,
}

export default Loading;

const styles = StyleSheet.create({
  contModal: {
    flex: 1,
    justifyContent: 'center',
    padding: 70,
    backgroundColor: colors.blackTransparent,
  },
  containerContentInAlert: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 20,
    alignItems:'center',
  },
});
