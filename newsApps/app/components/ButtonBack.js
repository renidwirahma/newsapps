import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import images from '../assets/images';
import colors from '../assets/colors';

class ButtonBack extends React.Component {

  handlePress = () => {
    this.props.onPress?.();
  };

  render() {
    return (
      <TouchableOpacity style={styles.bg} onPress={() => this.handlePress()}>
        <Image style={styles.icon} source={images.back}></Image>
      </TouchableOpacity>
    );
  }
}

export default ButtonBack;

const styles = StyleSheet.create({
  bg:{
    width:40,
    height:30,
    backgroundColor:colors.greyTransparent,
    justifyContent:'center',
    borderRadius:15,
    marginTop:20,
    marginLeft:10
  },
  icon:{
    width:22,
    height:22,
    resizeMode:'contain',
    alignSelf:'center',
  },
});
