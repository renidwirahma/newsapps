import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import colors from '../assets/colors';
import fonts from '../assets/fonts';

class ItemNews extends React.Component {
  handlePress = () => {
    this.props.onPress?.();
  };

  render() {
    return (
      <TouchableOpacity
        style={styles.wrapper}
        onPress={() => this.handlePress()}
        key={this.props.k}
        >
        <Image style={styles.img} source={{uri: this.props.img}}></Image>
        <View>
          <Text style={styles.title} ellipsizeMode={'tail'} numberOfLines={3}>
            {this.props.title}
          </Text>
          <View style={styles.row}>
            <Text style={styles.desc}>{this.props.source} | </Text>
            <Text style={styles.desc}>{this.props.date}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

ItemNews.defaultProps = {
  img: 'https://static01.nyt.com/images/2022/07/13/lens/13xp-flooding/13xp-flooding-facebookJumbo.jp',
  source: '',
  date: '2022-07-14T22:57:00Z',
  k: 'k',
};

export default ItemNews;

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.grey1,
  },
  img: {
    borderWidth: 1,
    width: '30%',
    height: 100,
    borderColor: colors.grey1,
    borderRadius: 10,
  },
  title: {
    color: 'black',
    fontFamily: fonts.semiBold,
    fontSize: 14,
    marginBottom: 5,
    maxWidth: '75%',
    marginLeft: 20,
  },
  desc: {
    color: colors.grey2,
    fontFamily: fonts.regular,
    fontSize: 11,
    marginBottom: 5,
    maxWidth: '75%',
  },
  row: {
    flexDirection: 'row',
    marginLeft: 20,
  },
});
